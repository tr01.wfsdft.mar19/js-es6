class Heroe{
    
    constructor(height, weight, live, attackDmg, name){
        this._height = height;
        this._weight = weight;
        this._live = live;
        this._attackDmg = attackDmg;
        this._name = name;
    }

    get stats(){
        return this.name + ' : ' + this.height + ' meters, ' + this.weight + ' kg';
    }

    set name(value){
        if(typeof value === 'string'){
            this._name = value;
        }
    }

    get name(){
        return this._name;
    }

    set height(value){
        if(typeof value === 'number'){
            this._height = value;
        }
    }

    get height(){
        return this._height;
    }

    set weight(value){
        if(typeof value === 'number'){
            this._weight = value;
        }
    }

    get weight(){
        return this._weight;
    }
}

class Mage extends Heroe{
    
    constructor(height, weight, live, attackDmg, name, role, mana){
        super(height, weight, live, attackDmg, name);
        this._role = role;
        this._mana = mana
    }

    get role(){
        return this._role;
    }

    set role(value){
        if(typeof value === "string"){
            this._role = value;
        }
    }

    attack(obj){
        //console.log("Mage starts casting with: " + this._mana);
        this._mana -= 10;
        //console.log(obj, obj._live);
        if(obj._live > 0 && obj._live > this._attackDmg){
            obj._live -= this._attackDmg;
        }else{
            obj._live = 0;
        }
        
        //console.log('Mage now has ' + this._live + ' live and ' + this._mana + ' mana.');
    }

    attacked(obj){
        obj.attack(this);
        if(this._live > obj._attackDmg){
            this._live -= obj._attackDmg;
        }else{
            this._live = 0;
        }
        
        console.log('Mage now has ' + this._live + ' live and ' + this._mana + ' mana.');
    }
}

class Warrior extends Heroe{
    
    constructor(height, weight, live, attackDmg, name, role, strength){
        super(height, weight, live, attackDmg, name);
        this._role = role;
        this._strength = strength;
    }

    get role(){
        return this._role;
    }

    set role(value){
        if(typeof value === "string"){
            this._role = value;
        }
    }

    get strength(){
        return this._strength;
    }

    set strength(value){
        if(typeof value === 'number'){
            this._strength -= value;
        }
        return this._strength;
    }

    attack(obj){
        //console.log('Warrior attacks with: ' + this._strength + ' strength');
        this._strength -= 10;
        //console.log(obj, obj._live);
        if(obj._live > 0 && obj._live > this._attackDmg){
            obj._live -= this._attackDmg;
        }else{
            obj._live = 0;
        }
        
        //console.log('Warrior has now: ' + this._live + ' live and ' + this._strength + ' strength');
    }

    attacked(obj){
        obj.attack(this);
        if(this._live > obj._attackDmg){
            this._live -= obj._attackDmg;
        }else{
            this._live = 0;
        }
        
    }
}

class MarksMan extends Heroe{

    constructor(height, weight, live, attackDmg, name, role, agility){
        super(height, weight, live, attackDmg, name);
        this._role = role;
        this._agility = agility; 
    }

    get role(){
        return this._role;
    }

    set role(value){
        if(typeof value === "string"){
            this._role = value;
        }
    }

    get agility(){
        return this._agility;
    }

    set agility(value){
        if(typeof value === "number"){
            this._agility = value;
        }
    }

    attack(obj){
        //console.log('Marksman shoots with ' + this.agility + ' agility');
        this._agility -= 10;
        //console.log(obj, obj._live);
        if(obj._live > 0 && obj._live > this._attackDmg){
            obj._live -= this._attackDmg;
        }else{
            obj._live = 0;
        }
        
        //console.log('Marksman now has ' + this._live + ' live and ' + this._agility + ' agility');
    }

    attacked(obj){
        obj.attack(this);
        if(this._live > obj._attackDmg){
            this._live -= obj._attackDmg;
        }else{
            this._live = 0;
        }
        
    }
}

const init = ()=>{

    let mage = new Mage(1.8, 70, 100, 40, "Ignis", "Harass", 100);
    let warrior = new Warrior(1.7, 60, 100, 70, "Butterfly", "Assassin", 100);
    let marksman = new MarksMan(1.9, 80, 100, 50, "Valhein", "MarksMan", 100);

    let chars = Array();
    chars.push({char: mage, src:'./assets/Ignis.png'});
    chars.push({char: warrior, src: './assets/Butterfly.png'});
    chars.push({char: marksman, src: './assets/Valhein.png'});

    let template = document.querySelector('#item');
    let clon;
    let i = 0;
    chars.forEach((json)=>{

        clon = template.content.cloneNode(true);
        let name = clon.querySelector('#name');
        name.id = json.char._name;
        name.textContent = json.char.name;
        let img = clon.querySelector('#img');
        img.src = json.src;

        let attributes = clon.querySelector('#attributes');

        let role = json.char._role;
        let live = json.char._live;
        let attackDmg = json.char._attackDmg;
        let spanRole = document.createElement('span');
        let spanLive = document.createElement('span');
        let spanAttackDmg = document.createElement('span');
        spanRole.innerHTML = 'Role: ' + role;
        spanLive.innerHTML = 'Live: ' + live;
        spanAttackDmg.innerHTML = 'Attack Damage: ' + attackDmg;

        let spanMana = document.createElement('span');
        let spanStrength = document.createElement('span');
        let spanAgility = document.createElement('span');
        if(json.char._mana){
            let mana = json.char._mana;
            spanMana.innerHTML = 'Mana: ' + mana;
        }
        if(json.char._strength){
            let strength = json.char._strength;
            spanStrength.innerHTML = 'Strength: ' + strength;
        }
        if(json.char._agility){
            let agility = json.char._agility;
            spanAgility.innerHTML = 'Agility: ' + agility;
        }

        let br = document.createElement('br');
        let br2 = document.createElement('br');
        let br3 = document.createElement('br');
        attributes.appendChild(spanRole);
        attributes.appendChild(br);
        attributes.appendChild(spanLive);
        attributes.appendChild(br2);
        attributes.appendChild(spanAttackDmg);

        if(json.char._mana){
            attributes.appendChild(br3);
            attributes.appendChild(spanMana);
        }
        if(json.char._strength){
            attributes.appendChild(br3);
            attributes.appendChild(spanStrength);
        }
        if(json.char._agility){
            attributes.appendChild(br3);
            attributes.appendChild(spanAgility);
        }

        let charsSelect = clon.querySelector('#chars');
        let name2 = json.char._name;
        let option;
        chars.forEach((json)=>{
            if(charsSelect !== null && json.char.name !== name2) {
                option = document.createElement('option');
                option.value = json.char.name;
                option.text = json.char.name;
                option.id = 'char';
                charsSelect.add(option);
            }
        });
        // attack
        let selectedValue;
        let charTobeAttacked;
        clon.querySelector('#attack').addEventListener('click', (e)=>{
            //console.log(e.target.parentNode.childNodes[3].value);
            selectedValue = e.target.parentNode.childNodes[3].value;
            //console.log(e.target.parentNode.parentNode.childNodes[3].innerText);
            let myChar = chars.find((json)=>{
                return json.char._name === e.target.parentNode.parentNode.childNodes[3].innerText;
            });
            //console.log(myChar.char);
            charTobeAttacked = chars.find((json)=>{
                return json.char._name === selectedValue;
            });
            //console.log(charTobeAttacked.char);
            myChar.char.attack(charTobeAttacked.char);
            //clon.querySelector('.progress-bar').style.width = charTobeAttacked.char._live;
            //console.log(charTobeAttacked.char);
            update(myChar, charTobeAttacked.char);
        });

        document.body.querySelector('#colItems').appendChild(clon);
    });

    function update(myChar, selectedValue){

        if(myChar.char._mana){
            //console.log(document.querySelector('#'+myChar.char._name).parentNode.childNodes[7].childNodes[1].childNodes[6].innerHTML);
            document.querySelector('#'+myChar.char._name).parentNode.childNodes[7].childNodes[1].childNodes[6].innerHTML = "Mana: " + myChar.char._mana;
        }else if(myChar.char._strength){
            document.querySelector('#'+myChar.char._name).parentNode.childNodes[7].childNodes[1].childNodes[6].innerHTML = "Strength: " + myChar.char._strength;
        }else if(myChar.char._agility){
            document.querySelector('#'+myChar.char._name).parentNode.childNodes[7].childNodes[1].childNodes[6].innerHTML = "Agility: " + myChar.char._agility;
        }

        let selectedLive = selectedValue._live;
        //console.log(document.querySelector('#'+selectedValue._name).parentNode);
        let parentSelected = document.querySelector('#'+selectedValue._name).parentNode;
        //console.log(parent.childNodes[7].childNodes[1].childNodes[2].innerHTML);
        //console.log(parentSelected.querySelector('.progress-bar').getAttribute('aria-valuenow'));
        /*parentSelected.querySelector('.progress-bar').setAttribute('aria-valuenow', selectedValue._live);
        parentSelected.querySelector('.progress-bar').style.width = selectedValue._live;*/
        parentSelected.querySelector('progress').value = selectedValue._live;

        parentSelected.childNodes[7].childNodes[1].childNodes[2].innerHTML = "Live: " + selectedLive;
        if(parentSelected.childNodes[7].childNodes[1].childNodes[2].innerHTML === "Live: 0"){
            document.body.querySelector('#colItems').removeChild(parentSelected);
        }
    }
};

init();


/*
let mage = new Mage(1.8, 70, 100, 40, "Ignis", "Harass", 1000);
let warrior = new Warrior(1.7, 60, 100, 70, "Butterfly", "Assassin", 100);
let marksman = new MarksMan(1.9, 80, 100, 50, "Valhein", "MarksMan", 90);


marksman.attacked(mage);
marksman.attacked(mage);
warrior.attack(mage);
warrior.attack(mage);
*/